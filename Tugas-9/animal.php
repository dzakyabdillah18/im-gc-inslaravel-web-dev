<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
    class Animal{
        public $name;
        public $legs;
        public $cold_blooded;

        /**
         * Class constructor.
         */
        public function __construct($name)
        {
            $legs = 4;
            $cold_blooded = "no";
            $this-> name= $name;
            $this-> legs = $legs;
            $this-> cold_blooded = $cold_blooded;

        }
    }
    
    ?>
</body>
</html>